package ru.chelnokov.tasksWithFractions;

/**
 * Класс для объекта дробь.
 * @author Chelnokov E.I.
 */

public class Fraction {
    private int numerator;
    private int denominator;

    public Fraction(int numerator, int denominator){
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction(int theWholePart,int numerator, int denominator){
        this.numerator = numerator + theWholePart * denominator;
        this.denominator = denominator;
    }

    public Fraction(int theWholePart){
        this.numerator = theWholePart;
        this.denominator = 1;
    }

    int getNumerator() {
        return numerator;
    }

    int getDenominator() {
        return denominator;
    }

    /**
     * Метод для корректного вывода дроби
     * @return
     */
    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
