package ru.chelnokov.tasksWithFractions;


/**
 * Класс для решения выражений с дробями вида <дробь><знак операции><дробь>
 *
 * Ответом на данное выражение является дробь
 *
 * Класс умеет выполнять четыре операции:
 *
 * сложение;
 * вычитание;
 * умножение;
 * деление.
 *
 * @author Chelnokov E.I.
 */
class Expression {


    /**
     * Приводит две исходные дроби к общему знаменателю
     * @param fraction исходная дробь
     * @param fraction1 исходная дробь
     * @return Массив из двух дробей с одинаковым знаменателем
     */
    private static Fraction[] toCommonDenominator(Fraction fraction, Fraction fraction1) {
        int i = 1;
        while (!((i % fraction.getDenominator() == 0) && (i % fraction1.getDenominator() == 0))){
            i++;
        }
        if ((fraction.getDenominator() != i) && (fraction1.getDenominator() != i)){
            fraction = new Fraction(fraction.getNumerator() * (i / fraction.getDenominator()), i);
        }
        return new Fraction[]{fraction, fraction1};
    }

    /**
     * Метод сложения дробей
     * @param fraction слагаемое
     * @param fraction1 слагаемое
     * @return сумма двух дробей в виде дроби
     */
    static Fraction add(Fraction fraction, Fraction fraction1) {
        Fraction[] fractions = new Fraction[0];
        if (fraction.getDenominator() != fraction1.getDenominator()){
            fractions = toCommonDenominator(fraction,fraction1);
        }
        Fraction answer = new Fraction(fractions[0].getNumerator() + fractions[1].getNumerator(), fractions[0].getDenominator());
        return reduction(answer);
    }

    /**
     * Метод вычитания двух дробей
     * @param fraction уменьшаемое
     * @param fraction1 вычитаемое
     * @return разность в виде дроби
     */
    static Fraction sub(Fraction fraction, Fraction fraction1) {
        Fraction[] fractions = new Fraction[0];
        if (fraction.getDenominator() != fraction1.getDenominator()){
            fractions = toCommonDenominator(fraction,fraction1);
        }
        Fraction answer =  new Fraction(fractions[0].getNumerator() - fractions[1].getNumerator(), fractions[0].getDenominator());
        return (reduction(answer));
    }

    /**
     * Метод для перемножения двух дробей
     * @param fraction множитель
     * @param fraction1 множитель
     * @return произведение в виде дроби
     */
    static Fraction multy(Fraction fraction, Fraction fraction1) {
        Fraction answer = new Fraction(fraction.getNumerator() * fraction1.getNumerator(), fraction.getDenominator() * fraction1.getDenominator());
        return reduction(answer);
    }

    /**
     * Метод деления двух дробей
     * @param fraction делимое
     * @param fraction1 делитель
     * @return частное в виде дроби
     */
    static Fraction div(Fraction fraction, Fraction fraction1) {
        Fraction answer = new Fraction(fraction.getNumerator() * fraction1.getDenominator(), fraction.getDenominator() *fraction1.getNumerator());
        return reduction(answer);
    }



    /**
     * Метод сокращения исходной дроби
     *
     * ещё в разработке
     * @param fraction исходная дробь
     * @return полученная сокращённая дробь
     */
    private static Fraction reduction(Fraction fraction){
        int numerator = Math.abs(fraction.getNumerator());
        int denominator = Math.abs(fraction.getDenominator());
        int min = Math.min(numerator, denominator);
        int max = Math.max(numerator, denominator);
        for (int i = Math.abs(min); i > 1; i-- ){
            if ((min % i == 0) && (max % i == 0)){
                return new Fraction(fraction.getNumerator() / i, fraction.getDenominator() / i);
            }
        }
        return fraction;
    }
}
