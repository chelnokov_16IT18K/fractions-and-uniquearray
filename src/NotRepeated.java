import java.util.Arrays;
import java.util.LinkedHashSet;

/**
 * Выводит из исходного массива уникальные данные
 *
 * @author Chelnokov E.I., 16IT18K
 */
public class NotRepeated {
    public static void main(String[] args) {
        String[] array = {"t","o"," ","d","i","e","t"};
        LinkedHashSet<String> unique = new LinkedHashSet<>();
        unique.addAll(Arrays.asList(array));
        System.out.println(unique );
    }
}
