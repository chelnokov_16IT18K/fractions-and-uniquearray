package ru.chelnokov.tasksWithFractions;

import static ru.chelnokov.tasksWithFractions.Expression.*;

/**
 * Демокласс для решения примеров с дробями
 * @author Chelnokov E.I., 16IT18K
 */

public class Demo {
    public static void main(String[] args) {
        String expression = "1/4 - 4/8";
        String[] partsOfExpression = expression.split(" ");
        Fraction fraction = getFraction(partsOfExpression[0]);
        Fraction fraction1 = getFraction(partsOfExpression[2]);

        switch (partsOfExpression[1]) {
            case "+":
                System.out.println(fraction + " + " + fraction1 + " = " +
                        add(fraction, fraction1));
                break;
            case "-":
                System.out.println(fraction + " - " + fraction1 + " = " +
                        sub(fraction, fraction1));
                break;
            case "*":
                System.out.println(fraction + " * " + fraction1 + " = " +
                        multy(fraction, fraction1));
                break;
            case "/":
                System.out.println(fraction + " / " + fraction1 + " = " +
                        div(fraction, fraction1));
                break;
            default:
                System.out.println("Мы с таким не работаем(");
        }
    }
    /**
     * Метод, возвращающий объект типа дробь из исходной строки
     * @param part дробь, записанная в формате строки
     * @return объект типа дробь, над которым можно выполнять дальнейшие вычисления
     */
    private static Fraction getFraction(String part) {
        String[] fr = part.split("/");
        return new Fraction(Integer.valueOf(fr[0]),Integer.valueOf(fr[1]));
    }

}
